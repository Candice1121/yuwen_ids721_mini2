# Yuwen_IDS721_mini2

This is a deployment presentation for AWS Lambda function with add function



## Lambda Functionality

1. This function perform addition for two input


## Install

### Install Rust ( rust-lang.org/tools/install )

1.  ```curl --proto '=https' --tlsv1.2 -sSf https://sh.rustup.rs | sh```

### Install Cargo Lambda (https://www.cargo-lambda.info/guide/getting-started.html)

1. brew tap cargo-lambda/cargo-lambda
2. brew install cargo-lambda

### Basic Commands

1. ```cargo lambda new projectName```
2. ```cargo lambda watch```
3. ```cargo lambda invoke ... ```
4. ```cargo lambda build --release```

## Deploy

1. Create a AWS account
2. Go to IAM service, create access key and save the ```access key ID``` and ```secret access key```
3. Set up AWS toolkit in VSCode
4. Set up AWS CLI using credentials from access key

```
[default credentials file]
aws_access_key_id = your_access_key_ID
aws_secret_access_key = your_secret_access_key
```

5. Under your project: ```cargo lambda deploy --region your_region ```
6. Then you can find the lambda function in your AWS Explorer in VS Code

[deploy](imgs/deploy.png)


## Invoke
1. Use ```make invoke ``` presented in Makefile and see the log file on AWS account.

[invoke](imgs/invoke.png)
